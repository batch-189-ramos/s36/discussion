
// Import the Task model from the models folder by using the 'require' directive
const Task = require('../models/Task.js')

// Retrieve all the tasks inside the MongoDb collection
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// Creates a new task on the MongoDb collection
module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})
	return newTask.save().then((savedTask, error) => {
		if(error) {
			return error
		}
		return 'Task created successfully!'
	})
}	


// Updates a task on the MongoDb collection
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			return(error)
		}
		result.name = newContent.name
		return result.save().then((updatedTask, error) => {
			if(error) {
				return(error)
			}
			return updatedTask
		})
	})
}



// Delete a task from the MongoDb collection
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
		if(error) {
			return(error)
		}
		return deletedTask
	})
}


// Retrieves a task from the MongoDb collection
module.exports.retrieveTask = (taskId) => {
	return Task.findById(taskId).then((retrievedTask, error) => {
		if(error) {
			return(error)
		}
		return retrievedTask
	})
}