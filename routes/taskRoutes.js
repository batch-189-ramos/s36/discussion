
// Import express and any controllers using the 'require' directive.
const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()

// Route for /api/tasks/ - runs the getAllTasks function from the controller.
router.get('/', (req, res) => {
	TaskController.getAllTasks().then((tasks) => res.send(tasks))
})


// Route for /api/tasks/create - runs the createTask function from the controller.
router.post('/create', (req, res) =>{
	TaskController.createTask(req.body).then((task) => res.send(task))
})


// Route for /api/tasks/update - runs the updateTask function from the controller.
// You can use a parameter variable within the endpoint by using a ':' and the name of the parameter. Ex.: ':id'
router.put('/:id/update', (req, res) => {
	// updateTask requires 2 argumerns:
	/*
		1. ID of the task to be updated
		2. New content of the task
	*/
	TaskController.updateTask(req.params.id, req.body).then((updatedTask) => res.send(updatedTask))
})


// Route for /api/tasks/delete - runs the deleteTask function from the controller.
router.delete('/:id/delete', (req, res) => {
	TaskController.deleteTask(req.params.id).then((deletedTask) => res.send(deletedTask))
})


// Route for /api/tasks/retrieve-task - runs the retrieveTask function from the controller.
router.get('/:id/retrieve-task', (req, res) => {
	TaskController.retrieveTask(req.params.id).then((retrievedTask) => res.send(retrievedTask))
})

module.exports = router